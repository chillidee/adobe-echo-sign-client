﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ClientApp for Adobe Echo Sign")]
[assembly: AssemblyDescription("Client that consumes Adobe Echo Sign API")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Chilli Dee")]
[assembly: AssemblyProduct("ClientApp")]
[assembly: AssemblyCopyright("Copyright ©  2018-2021 - CHILLI DEE - THE DIGITAL MARKETING GROUP")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9562ee02-bfc3-4a24-a994-ac50f02c8881")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.4.0.0")]
[assembly: AssemblyFileVersion("1.4.0.0")]
