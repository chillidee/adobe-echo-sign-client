﻿namespace ClientApp.Classes
{
    public class ConstantsHelper
    {
        public static class CommandLine
        {
            public const int ACTION = 0;
            public const int JSON = 1;
        }

        public static class AppConfig
        {
            public const string LOG_PATH = "logPath";
            public const string AGREEMENTS_PATH = "agreementsPath";
            public const string CONNECTION_STRING_MM = "connectionStringMM";
            public const string CONNECTION_STRING_CLEAN_CREDIT_MM = "connectionStringCleanCreditMM";
            public const string CONNECTION_STRING_Marketing_MM = "connectionStringMarketingMM";

            //Adobe Echo Sign
            public const string API_URL = "apiUrl";
            public const string CLIENT_ID = "clientId";
            public const string SECRET_ID = "secretId";
            public const string REFRESH_TOKEN = "refreshToken";

            //Adobe Echo Sign | Library Template & E-mail
            public const string LIBRARY_DOCUMENT_ID_CC = "libraryDocumentIdCC";
            public const string LIBRARY_DOCUMENT_ID_CDR = "libraryDocumentIdCDR";
            public const string LIBRARY_DOCUMENT_ID_JB = "libraryDocumentIdJB";
            public const string LIBRARY_DOCUMENT_NAME = "libraryDocumentName";
            public const string EMAIL_MESSAGE = "emailMessage";

            //SMS via Esendex
            public const string SMS_ACTIVE = "smsActive";
            public const string SMS_REFERENCE = "smsReference";
            public const string SMS_USER_NAME = "smsUserName";
            public const string SMS_PASSWORD = "smsPassword";
            public const string SMS_SENDER_CC = "smsSenderCC";
            public const string SMS_MESSAGE_CC = "smsMessageCC";
            public const string SMS_SENDER_CDR = "smsSenderCDR";
            public const string SMS_MESSAGE_CDR = "smsMessageCDR";
            public const string SMS_MESSAGE_JB = "smsMessageJB";
            public const string SMS_SENDER_JB = "smsSenderJB";            

            //Bitly
            public const string BITLY_ACTIVE = "bitlyActive";
            public const string BITLY_URL = "bitlyUrl";
            public const string BITLY_LOGIN = "bitlyLogin";
            public const string BITLY_API_KEY = "bitlyApiKey";
        }

        public static class Action
        {
            public const string GET_LIBRARY_DOCUMENTS = "GetLibraryDocuments";
            public const string SEND_AGREEMENT = "SendAgreement";
            public const string GET_FORM_DATA = "GetFormData";
            public const string GET_AGREEMENT_STATUS = "GetAgreementStatus";

            // Special action to inform salespeople about any new leads that came in
            public const string SEND_SMS = "SendSMS";            
        }

        public enum Company
        {
            CLEAN_CREDIT = 14,
            CLEAN_CREDIT_DEBT_RESOLUTIONS = 17,
            JUST_BUDGET = 19
        }
    }
}
