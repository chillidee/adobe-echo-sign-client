﻿using com.esendex.sdk.messaging;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.Classes
{
    public class SyncEsendex
    {
        public static bool DEBUG = false;

        public static async Task Execute()
        {
            try
            {
                LogHelper.WriteLog("2. Verifying new leads...", true, 1);

                DataTable data = new DataTable();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_MM]))
                {

                    StringBuilder sql = new StringBuilder();
                    sql.Append(" SELECT * ");
                    sql.Append(" FROM vwCallSMS ");
                    sql.Append(" WHERE SentDate IS NULL ");                    
                    sql.Append(" ORDER BY ReceivedDate ASC");

                    SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                    conn.Open();

                    data.Load(cmd.ExecuteReader());
                }

                if (data.Rows.Count > 0)
                {
                    LogHelper.WriteLog(string.Format("It has been found {0} lead(s).", data.Rows.Count), false, 2);

                    string sender = string.Empty;
                    string message = string.Empty;
                    string typeOfSMS = string.Empty;
                    string[] sendTo = null;

                    foreach (DataRow row in data.Rows)
                    {
                        message = row["Message"].ToString();

                        // Default SMS
                        if (string.IsNullOrEmpty(message))
                        {
                            sender = "MoneyMaker";

                            // Clean Credit Leads
                            if (row["CompanyID"].ToString().Equals("14"))
                            {
                                LogHelper.WriteLog(string.Format("Sending SMS - New CC lead {0} received in MM.", row["CallID"].ToString()), true, 3);
                                typeOfSMS = "CC";
                            }
                            else // Property Leads
                            {
                                LogHelper.WriteLog(string.Format("Sending SMS - New Property lead {0} received in MM.", row["CallID"].ToString()), true, 3);
                                typeOfSMS = "Property";
                            }

                            message = string.Format("New {0} lead received in MM on {1} at {2}. Customer {3} | Mobile {4} | Email {5}",
                                        typeOfSMS,
                                        string.Format("{0:dd/MM/yyyy}", DateTime.Parse(row["ReceivedDate"].ToString())),
                                        string.Format("{0:hh:mm:ss tt}", DateTime.Parse(row["ReceivedDate"].ToString())),
                                        RemoveSpecialCharacters(row["FirstName"].ToString().Trim()),
                                        RemoveSpecialCharacters(row["Mobile"].ToString().Trim()),
                                        row["Email"].ToString().Trim().Replace("'", ""));
                        }
                        else
                        {
                            // Custom SMS - via DB, it's ready to send, there's no need to do anything else
                            typeOfSMS = "Custom";
                            sender = GetCompanyAcronym((int)row["CompanyID"]);
                        }

                        sendTo = row["SendTo"].ToString().Split(',');

                        if (!DEBUG)
                        {
                            foreach (string mobile in sendTo)
                                sendSMS(mobile, message, sender);
                        }

                        LogHelper.WriteLog("Updating sent date for lead.", false, 3);

                        // Update sent date
                        try
                        {
                            if (typeOfSMS.Equals("Custom"))
                                UpdateSentDate(ref data, row["CallID"].ToString());
                            else
                                UpdateSentDate(ref data, message, row["CallID"].ToString());
                        }
                        catch (Exception)
                        {
                            UpdateSentDate(ref data, "- Message could not be saved -", row["CallID"].ToString());
                        }

                        LogHelper.WriteLog("Updated sent date for lead.", false, 3);
                    }
                }
                else
                {
                    LogHelper.WriteLog("No leads have been found.", false, 2);
                }
            }
            catch (Exception error)
            {
                LogHelper.WriteLogSeparator();
                LogHelper.WriteLog(error.Message, false, 1);
                LogHelper.WriteLogSeparator();
            }
        }

        private static string GetCompanyAcronym(int companyID)
        {
            string acronym = string.Empty;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_MM]))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT Acronym ");
                sql.Append("FROM Company ");
                sql.AppendFormat("WHERE CompanyID = {0} ", companyID);

                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                conn.Open();
                var obj = cmd.ExecuteScalar();
                if (obj != null)
                    acronym = obj.ToString();
                else
                    acronym = "ALC";
            }

            return acronym;
        }

        private static void UpdateSentDate(ref DataTable data, string message, string callID)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_MM]))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(" UPDATE CallSMS ");
                sql.AppendFormat(" SET SentDate = GETDATE(), Message = '{0}' ", message);
                sql.AppendFormat(" WHERE CallID = {0} ", callID);

                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                conn.Open();
                data.Load(cmd.ExecuteReader());
            }
        }

        private static void UpdateSentDate(ref DataTable data, string callID)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_MM]))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(" UPDATE CallSMS ");
                sql.AppendFormat(" SET SentDate = GETDATE() ");
                sql.AppendFormat(" WHERE CallID = {0} ", callID);

                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                conn.Open();
                data.Load(cmd.ExecuteReader());
            }
        }

        public static void sendSMS(string mobile, string message, string sender = "")
        {
            var messagingService = new MessagingService(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.SMS_USER_NAME],
                                                        ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.SMS_PASSWORD]);

            var msg = new SmsMessage(mobile, message, ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.SMS_REFERENCE]);

            if (!sender.Equals(string.Empty))
                msg.Originator = sender;

            messagingService.SendMessage(msg);
        }


        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == ' ')
                    sb.Append(c);
                else
                    sb.Append(" ");
            }
            return sb.ToString();
        }
    }
}