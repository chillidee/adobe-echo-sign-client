﻿using System;
using System.IO;

namespace ClientApp.Classes
{
    public class LogHelper
    {
        public static string LogPath { get; set; }

        private static string LogName()
        {
            return "ClientApp_" + DateTime.Today.ToString("yyyy_MM_dd") + "." + "log";
        }

        private static string LogFile()
        {
            return Path.Combine(LogPath, LogName());
        }

        public static void WriteLogSeparator(bool lineBreak = false)
        {
            WriteLog(new String('*', 50), lineBreak);
        }

        public static void WriteLog(string text, bool lineBreak = false, int nTabs = 0)
        {
            FileInfo logFileInfo = new FileInfo(LogFile());
            DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

            if (!logDirInfo.Exists) logDirInfo.Create();

            using (FileStream fileStream = new FileStream(LogFile(), FileMode.Append))
            {
                using (StreamWriter log = new StreamWriter(fileStream))
                {
                    if (lineBreak)
                        log.WriteLine();

                    if (nTabs > 0)
                    {
                        log.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + new String('\t', nTabs) + text);
                        Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + new String('\t', nTabs) + text);
                    }
                    else
                    {
                        log.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + text);
                        Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " - " + text);
                    }
                }
            }
        }
    }
}
