﻿using EchosignRESTClient;
using EchosignRESTClient.Models;
using EchosignRESTClient.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IO = System.IO;

namespace ClientApp.Classes
{
    public class SyncAdobeEchoSign
    {
        public static async Task Execute(string action, JsonData jsonData)
        {
            try
            {
                //https://au1.documents.adobe.com/account/accountSettingsPage#pageId::API_APPLICATIONS
                LogHelper.WriteLog("2. Connecting to Adobe Echo Sign...", true, 1);
                EchosignREST client = new EchosignREST(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.API_URL], ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CLIENT_ID], ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.SECRET_ID]);
                LogHelper.WriteLog("Connection established successfully.", false, 2);

                //1. Use this link to obtain the Access Token
                //https://secure.au1.echosign.com/public/oauth?redirect_uri=https://www.cleancredit.com.au/&response_type=code&client_id=CBJCHBCAABAAfRo4MbTRlqKLlHE3SOUVpVcTS49RojD_&scope=user_login:self+user_write:self+library_read:self+agreement_write:self+agreement_read:self&state=                
                //https://secure.au1.echosign.com/public/oauth?redirect_uri=https://www.justbudget.com.au/&response_type=code&client_id=CBJCHBCAABAAfhvQ1D1iCoDHkNRCeRO_nDgSXWvoP1ok&scope=user_login:self+user_write:self+library_read:self+agreement_write:self+agreement_read:self
                //https://www.justbudget.com.au/?code=CBNCKBAAHBCAABAAOIaDJx6oxPZcGH6sv7LD0Qkv8JDv8hQ0&api_access_point=https%3A%2F%2Fapi.au1.documents.adobe.com%2F&web_access_point=https%3A%2F%2Fau1.documents.adobe.com%2F

                //2. Allow access and copy the Access Token here to obtain the Refresh Token
                //await client.Authorize("CBNCKBAAHBCAABAAOIaDJx6oxPZcGH6sv7LD0Qkv8JDv8hQ0", "https://www.justbudget.com.au/");                               

                //3. If you have the Refresh Token, you do not need these steps anymore - It is just a friendly reminder, just in case

                // Gets authorization in order to proceed
                LogHelper.WriteLog("3. Authenticating account on Adobe Echo Sign...", true, 1);
                await client.Authorize(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.REFRESH_TOKEN]);
                LogHelper.WriteLog("Account authenticated successfully.", false, 2);

                LogHelper.WriteLog("4. Executing actions in Adobe Echo Sign API...", true, 1);

                // Use this to obtain the Library Document ID
                if (string.Equals(action, ConstantsHelper.Action.GET_LIBRARY_DOCUMENTS, StringComparison.OrdinalIgnoreCase))
                {
                    await getLibraryDocuments(client);
                }
                else if (string.Equals(action, ConstantsHelper.Action.SEND_AGREEMENT, StringComparison.OrdinalIgnoreCase))
                {
                    await sendAgreement(client, jsonData);
                }
                else if (string.Equals(action, ConstantsHelper.Action.GET_FORM_DATA, StringComparison.OrdinalIgnoreCase))
                {
                    await getFormData(client);
                }
                else if (string.Equals(action, ConstantsHelper.Action.GET_AGREEMENT_STATUS, StringComparison.OrdinalIgnoreCase))
                {
                    await getAgreementStatus(client);
                }
                else
                {
                    LogHelper.WriteLog(string.Format("Action '{0}' especified has not matched with '{1}' or '{2}' actions. ", action, ConstantsHelper.Action.GET_LIBRARY_DOCUMENTS, ConstantsHelper.Action.SEND_AGREEMENT), false, 2);
                }
            }
            catch (Exception error)
            {
                LogHelper.WriteLogSeparator();
                LogHelper.WriteLog(error.Message, false, 1);
                LogHelper.WriteLogSeparator();
            }
        }

        private static async Task getLibraryDocuments(EchosignREST client)
        {
            LogHelper.WriteLog(string.Format("[{0}] Retrieving all Library Documents...", ConstantsHelper.Action.GET_LIBRARY_DOCUMENTS), true, 2);

            //Retrieve all Library Documents
            DocumentLibraryItems library = await client.GetLibraryDocuments();

            if (library != null && library.libraryDocumentList != null && library.libraryDocumentList.Length > 0)
            {
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");

                var newList = library.libraryDocumentList.OrderByDescending(x => x.modifiedDate).ToList();
                LogHelper.WriteLog("Latest Library Document found:", false, 3);
                LogHelper.WriteLog(string.Format("ID: {0}", newList[0].id), false, 4);
                LogHelper.WriteLog(string.Format("Name: {0}", newList[0].name), false, 4);
                LogHelper.WriteLog(string.Format("Date: {0}", TimeZoneInfo.ConvertTime(newList[0].modifiedDate, timeZone).ToString("dd/MM/yyyy hh:mm:ss tt")), false, 4);
            }
            else
            {
                LogHelper.WriteLog("Library Documents not found, please create document in Dashboard > Add Template to Library.", false, 3);
            }
        }

        private static async Task sendAgreement(EchosignREST client, JsonData jsonData)
        {
            String agreementId = String.Empty;

            LogHelper.WriteLog(string.Format("[{0}] Sending Agreement Document...", ConstantsHelper.Action.SEND_AGREEMENT), true, 2);

            if (jsonData != null && jsonData.memberInfos != null && jsonData.mergeFieldInfo != null)
            {
                MergefieldInfo customer = jsonData.mergeFieldInfo.Find(i => i.fieldName == "customerGivenName");

                MergefieldInfo customerReference = jsonData.mergeFieldInfo.Find(i => i.fieldName == "customerReference");

                int companyID = jsonData.companyID;

                // Uses Library Document to create Agreement
                SendAgreementRequest agreementRequest = new SendAgreementRequest()
                {
                    name = ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.LIBRARY_DOCUMENT_NAME],
                    message = string.Format(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.EMAIL_MESSAGE], customer.defaultValue),
                    signatureType = nameof(Enums.SignatureType.ESIGN),
                    state = nameof(Enums.AgreementStatus.IN_PROCESS),
                    ccs = jsonData.ccs,
                    participantSetsInfo = new List<ParticipantSetInfo>() {
                                new ParticipantSetInfo() {
                                    role =  nameof(Enums.ParticipantRole.SIGNER),
                                    order = 1,
                                    memberInfos = new List<ParticipantInfo> {
                                        new ParticipantInfo() {
                                            email = jsonData.memberInfos[0].email
                                        }
                                    }
                                }
                            },
                    fileInfos = new List<FileInfo>() {
                                new FileInfo() {
                                    libraryDocumentId = GetLibraryDocumentID(companyID)
                                }
                            },
                    mergeFieldInfo = jsonData.mergeFieldInfo
                };

                // There are 2 signers when it is CDR, which are the same
                if (companyID == (int)ConstantsHelper.Company.CLEAN_CREDIT_DEBT_RESOLUTIONS)
                    agreementRequest.participantSetsInfo.Add(agreementRequest.participantSetsInfo[0]);

                //Sends it to get signed by the customer
                AgreementCreationResponse agreement = await client.SendAgreement(agreementRequest);

                if (agreement != null)
                {
                    agreementId = agreement.Id;
                    LogHelper.WriteLog(string.Format("Agreement Id created: {0}", agreementId), false, 3);

                    try
                    {
                        //Saves a file named with the agreement id, just to keep track of everything that was sent
                        string agreementFilePath = IO.Path.Combine(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.AGREEMENTS_PATH], nameof(Enums.AgreementStatus.OUT_FOR_SIGNATURE), agreementId + ".txt");
                        IO.FileInfo agreementFileInfo = new IO.FileInfo(agreementFilePath);
                        IO.DirectoryInfo agreementDirInfo = new IO.DirectoryInfo(agreementFileInfo.DirectoryName);
                        if (!agreementDirInfo.Exists) agreementDirInfo.Create();

                        using (IO.FileStream fileStream = new IO.FileStream(agreementFilePath, IO.FileMode.Append))
                        {
                            using (IO.StreamWriter sw = new IO.StreamWriter(fileStream))
                            {
                                sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
                                sw.WriteLine(JsonConvert.SerializeObject(agreementRequest));
                            }
                        }

                        LogHelper.WriteLog(string.Format("Created file: {0}", agreementFilePath), false, 3);
                    }
                    catch (Exception error)
                    {
                        LogHelper.WriteLogSeparator();
                        LogHelper.WriteLog(error.Message, false, 3);
                        LogHelper.WriteLogSeparator();
                    }

                    LogHelper.WriteLog("5. Checking SMS settings...", true, 1);

                    //Send SMS to customer (via Esendex) *only for SendAgreement action
                    if (string.Equals(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.SMS_ACTIVE], "true", StringComparison.OrdinalIgnoreCase))
                    {
                        LogHelper.WriteLog("SMS will be sent shortly. ", true, 2);

                        if (jsonData != null && jsonData.mergeFieldInfo != null)
                        {
                            MergefieldInfo customerGivenName = jsonData.mergeFieldInfo.Find(i => i.fieldName == "customerGivenName");
                            MergefieldInfo customerMobile = jsonData.mergeFieldInfo.Find(i => i.fieldName == "customerMobile");

                            LogHelper.WriteLog("Checking customer name and mobile phone...", false, 2);

                            if (customerGivenName != null && customerMobile != null && customerMobile.defaultValue.Length >= 9)
                            {
                                LogHelper.WriteLog("Checking agreement link...", false, 2);

                                //LogHelper.WriteLog(client.apiEndpointVer + "/agreements/" + agreementId + "/signingUrls", false, 2);                            

                                //Retrieve the url of the agreement, which has just sent
                                SigningUrlResponse document = null;

                                int nAttempts = 1;

                                //Unfortunately, it seems to be a race condition and retrying is likely the best option
                                while (document == null && nAttempts <= 10)
                                {
                                    try
                                    {
                                        LogHelper.WriteLog(string.Format("Attempt number '{0}'... ", nAttempts), false, 2);
                                        document = await client.GetSigningUrl(agreementId);
                                    }
                                    catch (Exception error)
                                    {
                                        LogHelper.WriteLog(error.Message, false, 2);
                                    }
                                    finally
                                    {
                                        nAttempts += 1;
                                        //We could not somehow get the url link as soon as we created the document, 
                                        //Adobe accused agreement Id invalid Or Do not need to be signed, how silly is that?
                                        Thread.Sleep(1000);
                                    }
                                }

                                if (document != null & document.signingUrlSetInfos != null && document.signingUrlSetInfos.Count > 0 &&
                                    document.signingUrlSetInfos[0].signingUrls != null && document.signingUrlSetInfos[0].signingUrls.Count > 0)
                                {
                                    string esignUrl = document.signingUrlSetInfos[0].signingUrls[0].esignUrl;

                                    LogHelper.WriteLog(string.Format("Agreement link: {0}", esignUrl), false, 3);

                                    //Make the url from Adobe Echo Sign shorter via Bitly
                                    if (string.Equals(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.BITLY_ACTIVE], "true", StringComparison.OrdinalIgnoreCase))
                                    {
                                        LogHelper.WriteLog("Making agreement link shortener via Bitly...", false, 3);

                                        try
                                        {
                                            String shortUrl = SyncBitly.GetBitly(esignUrl);
                                            if (!string.IsNullOrEmpty(shortUrl))
                                            {
                                                esignUrl = shortUrl;
                                                LogHelper.WriteLog(string.Format("Agreement Bitly link: {0}", esignUrl), false, 3);
                                            }
                                            else
                                            {
                                                LogHelper.WriteLog("It was not possible get Bitly link.", false, 3);
                                            }
                                        }
                                        catch (Exception error)
                                        {
                                            LogHelper.WriteLogSeparator();
                                            LogHelper.WriteLog(error.Message, false, 4);
                                            LogHelper.WriteLogSeparator();
                                        }
                                    }

                                    LogHelper.WriteLog(string.Format("Sending SMS to {0}...", customerMobile.defaultValue), false, 2);

                                    string smsMessage = string.Format(GetSMSMessage(companyID), customerGivenName.defaultValue, esignUrl);

                                    //Send the SMS via Esendex
                                    SyncEsendex.sendSMS(customerMobile.defaultValue, smsMessage, GetSMSSender(companyID));

                                    LogHelper.WriteLog(string.Format("Message: {0}", smsMessage), false, 2);
                                }
                                else
                                {
                                    LogHelper.WriteLog("Agreement link not found.", false, 2);
                                }
                            }
                            else
                            {
                                LogHelper.WriteLog("Fields 'customerGivenName' and 'customerMobile' not found, please inform them in mergeFieldInfo.", true, 2);
                            }
                        }
                        else
                        {
                            LogHelper.WriteLog("Please inform mergeFieldInfo (fields) before sending SMS.", false, 2);
                        }
                    }
                    else
                    {
                        LogHelper.WriteLog(string.Format("SMS will not be sent, unless you change the parameter '{0}' to 'true'.", ConstantsHelper.AppConfig.SMS_ACTIVE), false, 2);
                    }
                }
            }
            else
            {
                LogHelper.WriteLog("Please inform memberInfos (signer) and/or mergeFieldInfo (fields) before executing this action.", false, 3);
            }
        }
        private static string GetLibraryDocumentID(int companyID)
        {
            string documentID = string.Empty;

            switch (companyID)
            {
                case (int)ConstantsHelper.Company.CLEAN_CREDIT:
                    documentID = ConstantsHelper.AppConfig.LIBRARY_DOCUMENT_ID_CC;
                    break;
                case (int)ConstantsHelper.Company.CLEAN_CREDIT_DEBT_RESOLUTIONS:
                    documentID = ConstantsHelper.AppConfig.LIBRARY_DOCUMENT_ID_CDR;
                    break;
                case (int)ConstantsHelper.Company.JUST_BUDGET:
                    documentID = ConstantsHelper.AppConfig.LIBRARY_DOCUMENT_ID_JB;
                    break;
            }
            return ConfigurationManager.AppSettings[documentID];
        }

        private static string GetSMSMessage(int companyID)
        {
            string smsMessage = string.Empty;

            switch (companyID)
            {
                case (int)ConstantsHelper.Company.CLEAN_CREDIT:
                    smsMessage = ConstantsHelper.AppConfig.SMS_MESSAGE_CC;
                    break;
                case (int)ConstantsHelper.Company.CLEAN_CREDIT_DEBT_RESOLUTIONS:
                    smsMessage = ConstantsHelper.AppConfig.SMS_MESSAGE_CDR;
                    break;
                case (int)ConstantsHelper.Company.JUST_BUDGET:
                    smsMessage = ConstantsHelper.AppConfig.SMS_MESSAGE_JB;
                    break;
            }
            return ConfigurationManager.AppSettings[smsMessage];
        }

        private static string GetSMSSender(int companyID)
        {
            string smsSender = string.Empty;

            switch (companyID)
            {
                case (int)ConstantsHelper.Company.CLEAN_CREDIT:
                    smsSender = ConstantsHelper.AppConfig.SMS_SENDER_CC;
                    break;
                case (int)ConstantsHelper.Company.CLEAN_CREDIT_DEBT_RESOLUTIONS:
                    smsSender = ConstantsHelper.AppConfig.SMS_SENDER_CDR;
                    break;
                case (int)ConstantsHelper.Company.JUST_BUDGET:
                    smsSender = ConstantsHelper.AppConfig.SMS_SENDER_JB;
                    break;
            }
            return ConfigurationManager.AppSettings[smsSender];
        }

        private static bool IsCC(String customerReference)
        {
            int companyID = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_CLEAN_CREDIT_MM]))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT CompanyID ");
                sql.Append("FROM CF.dbo.Applicant ");
                sql.AppendFormat("WHERE ApplicantID = {0} ", customerReference);

                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                conn.Open();
                var obj = cmd.ExecuteScalar();
                if (obj != null)
                    companyID = (int)obj;
                else
                    companyID = 0;
            }

            return companyID == 14;
        }

        private static async Task getFormData(EchosignREST client)
        {
            LogHelper.WriteLog("Getting list of agreements out for signature...", true, 2);

            // Get the list of agreements out for signature
            String[] agreementsOutForSignature = IO.Directory.GetFiles(IO.Path.Combine(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.AGREEMENTS_PATH], nameof(Enums.AgreementStatus.OUT_FOR_SIGNATURE)));

            if (agreementsOutForSignature.Length > 0)
            {
                LogHelper.WriteLog(string.Format("Found {0} agreement(s) out for signature.", agreementsOutForSignature.Length), false, 3);

                string agreementId = string.Empty;

                foreach (string agreementOutForSignature in agreementsOutForSignature)
                {
                    agreementId = IO.Path.GetFileNameWithoutExtension(agreementOutForSignature);

                    DateTime dateAgreement = Convert.ToDateTime(IO.File.ReadLines(agreementOutForSignature).First());

                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                    dateAgreement = TimeZoneInfo.ConvertTime(dateAgreement, timeZone);

                    LogHelper.WriteLog(string.Format("Checking agreement {0}...", agreementId), true, 4);

                    //Retrieve data from agreement
                    AgreementInfo agreementInfo = await client.GetAgreement(agreementId);

                    if (agreementInfo != null)
                    {
                        LogHelper.WriteLog(string.Format("Current Status: {0}", agreementInfo.status), false, 4);

                        //Check if the agreement has been signed 
                        if (agreementInfo.status == nameof(Enums.AgreementStatus.SIGNED))
                        {
                            LogHelper.WriteLog("Checking fields filled out in the document...", false, 4);

                            //Pull data from the fields filled out
                            String formData = await client.GetFormData(agreementId);

                            if (!string.IsNullOrEmpty(formData))
                            {
                                string[] formFields = formData.Split(',');

                                if (formFields.Length > 29)
                                {
                                    String applicantID = string.Empty;
                                    String accountHolderName = string.Empty;
                                    String accountBSB = string.Empty;
                                    String accountNumber = string.Empty;

                                    // These numbers [x] change if the template has changed or replaced, there's no pattern, if you find...great!
                                    if (dateAgreement > new DateTime(2020, 5, 12, 5, 0, 0))
                                    {
                                        applicantID = formFields[73].Replace("\"", "");
                                        accountHolderName = formFields[56].Replace("\"", "");
                                        accountBSB = formFields[55].Replace("\"", "");
                                        accountNumber = formFields[57].Replace("\"", "");
                                    }
                                    else if (dateAgreement > new DateTime(2020, 3, 18, 12, 0, 0))
                                    {
                                        applicantID = formFields[69].Replace("\"", "");
                                        accountHolderName = formFields[52].Replace("\"", "");
                                        accountBSB = formFields[51].Replace("\"", "");
                                        accountNumber = formFields[53].Replace("\"", "");
                                    }
                                    else if (dateAgreement > new DateTime(2020, 3, 9, 9, 0, 0))
                                    {
                                        applicantID = formFields[65].Replace("\"", "");
                                        accountHolderName = formFields[48].Replace("\"", "");
                                        accountBSB = formFields[47].Replace("\"", "");
                                        accountNumber = formFields[49].Replace("\"", "");
                                    }
                                    else if (dateAgreement > new DateTime(2019, 7, 11, 9, 30, 0))
                                    {
                                        applicantID = formFields[39].Replace("\"", "");
                                        accountHolderName = formFields[31].Replace("\"", "");
                                        accountBSB = formFields[30].Replace("\"", "");
                                        accountNumber = formFields[32].Replace("\"", "");
                                    }
                                    else if (formFields.Length > 31)
                                    {
                                        applicantID = formFields[31].Replace("\"", "");
                                        accountHolderName = formFields[24].Replace("\"", "");
                                        accountBSB = formFields[23].Replace("\"", "");
                                        accountNumber = formFields[25].Replace("\"", "");
                                    }
                                    else // Old template, without DOB
                                    {
                                        applicantID = formFields[29].Replace("\"", "");
                                        accountHolderName = formFields[23].Replace("\"", "");
                                        accountBSB = formFields[22].Replace("\"", "");
                                        accountNumber = formFields[24].Replace("\"", "");
                                    }

                                    LogHelper.WriteLog("Updating data in Money Maker...", false, 4);

                                    if (!string.IsNullOrEmpty(accountHolderName) && !string.IsNullOrEmpty(accountBSB) && !string.IsNullOrEmpty(accountNumber))
                                    {
                                        //Update accountBSB, accountHolderName, accountNumber in MM
                                        using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_CLEAN_CREDIT_MM]))
                                        {
                                            StringBuilder sql = new StringBuilder();
                                            sql.Append("UPDATE Applicant SET ");
                                            sql.Append("AccountName = @AccountName, ");
                                            sql.Append("AccountBSB = @AccountBSB, ");
                                            sql.Append("AccountNo = @AccountNo ");
                                            sql.Append("WHERE ApplicantID = @ApplicantID AND ");
                                            sql.Append("((AccountBSB IS NULL OR LEN(AccountBSB) = 0) OR (AccountNo IS NULL OR LEN(AccountNo) = 0))");

                                            SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                                            cmd.Parameters.Add(new SqlParameter("ApplicantID", applicantID));
                                            cmd.Parameters.Add(new SqlParameter("AccountName", truncate(accountHolderName, 32)));
                                            cmd.Parameters.Add(new SqlParameter("AccountBSB", truncate(accountBSB, 6)));
                                            cmd.Parameters.Add(new SqlParameter("AccountNo", truncate(accountNumber, 30)));

                                            conn.Open();
                                            cmd.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            //Move file from out for signed to signed
                            string agreementFilePath = IO.Path.Combine(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.AGREEMENTS_PATH], nameof(Enums.AgreementStatus.SIGNED));
                            if (!IO.Directory.Exists(agreementFilePath)) IO.Directory.CreateDirectory(agreementFilePath);

                            IO.File.Move(agreementOutForSignature, IO.Path.Combine(agreementFilePath, agreementId + ".txt"));
                            LogHelper.WriteLog(string.Format("Moved file from '{0}' to '{1}'.", agreementOutForSignature, IO.Path.Combine(agreementFilePath, agreementId + ".txt")), false, 4);
                        }
                    }
                }
            }
            else
            {
                LogHelper.WriteLog("There is no agreements out for signature.", false, 3);
            }
        }

        private static async Task getAgreementStatus(EchosignREST client)
        {
            LogHelper.WriteLog(string.Format("[{0}] Getting all Agreements Status...", ConstantsHelper.Action.GET_AGREEMENT_STATUS), true, 2);

            //Retrieve all Agreements
            UserAgreements userAgreements = await client.GetAgreements();

            if (userAgreements != null && userAgreements.userAgreementList != null && userAgreements.userAgreementList.Count > 0)
            {
                AgreementInfo agreementInfo = null;
                EventList eventList = null;

                // Adobe Echo Sign has been using (GTM-8:00) Pacific Time (US & Canada), 
                // so we have to convert it to Australia (Sydney, Melbourne, Canberra) Time
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");

                int applicantId = 0;
                int companyID = 0;

                foreach (UserAgreement agreement in userAgreements.userAgreementList)
                {
                    try
                    {
                        LogHelper.WriteLog("Agreement found:", true, 3);

                        LogHelper.WriteLog(string.Format("ID: {0}", agreement.Id), true, 4);

                        if (agreement.displayParticipantSetInfos != null && agreement.displayParticipantSetInfos.Count > 0 &&
                            agreement.displayParticipantSetInfos.First().displayUserSetMemberInfos != null && agreement.displayParticipantSetInfos.First().displayUserSetMemberInfos.Count > 0)
                            LogHelper.WriteLog(string.Format("Email: {0}", agreement.displayParticipantSetInfos.First().displayUserSetMemberInfos.First().email), false, 4);

                        LogHelper.WriteLog(string.Format("Created Date: {0}", TimeZoneInfo.ConvertTime(agreement.displayDate, timeZone).ToString("dd/MM/yyyy hh:mm:ss tt")), false, 4);

                        LogHelper.WriteLog("Extracting agreement details...", true, 4);

                        agreementInfo = await client.GetAgreement(agreement.Id);

                        if (agreementInfo != null)
                        {
                            eventList = await client.GetEvents(agreement.Id);

                            LogHelper.WriteLog(string.Format("Current Status: {0}", agreementInfo.status), false, 5);

                            // Get the applicant of this application by email - CC or CDR
                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_CLEAN_CREDIT_MM]))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append("SELECT TOP 1 ApplicantID ");
                                sql.Append("FROM CF.dbo.Applicant ");
                                sql.Append("WHERE Email = @CustomerEmail ");
                                sql.Append("ORDER BY ApplicantID DESC");

                                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                                if (agreementInfo.participantSetsInfo != null && agreementInfo.participantSetsInfo.Count > 0 &&
                                   agreementInfo.participantSetsInfo.First().memberInfos != null && agreementInfo.participantSetsInfo.First().memberInfos.Count > 0)
                                {
                                    cmd.Parameters.Add(new SqlParameter("CustomerEmail", agreementInfo.participantSetsInfo.First().memberInfos.First().email));
                                }
                                else
                                {
                                    cmd.Parameters.Add(new SqlParameter("CustomerEmail", agreement.displayParticipantSetInfos.First().displayUserSetMemberInfos.First().email));
                                }

                                conn.Open();
                                var obj = cmd.ExecuteScalar();
                                if (obj != null)
                                {
                                    applicantId = (int)obj;
                                    companyID = (int)ConstantsHelper.Company.CLEAN_CREDIT;
                                }
                                else
                                    applicantId = 0;
                            }

                            // Get the applicant of this application by email - JB
                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_MM]))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append("SELECT TOP 1 ApplicantID ");
                                sql.Append("FROM BMApplicant ");
                                sql.Append("WHERE Email = @CustomerEmail ");
                                sql.Append("ORDER BY ApplicantID DESC");

                                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                                if (agreementInfo.participantSetsInfo != null && agreementInfo.participantSetsInfo.Count > 0 &&
                                   agreementInfo.participantSetsInfo.First().memberInfos != null && agreementInfo.participantSetsInfo.First().memberInfos.Count > 0)
                                {
                                    cmd.Parameters.Add(new SqlParameter("CustomerEmail", agreementInfo.participantSetsInfo.First().memberInfos.First().email));
                                }
                                else
                                {
                                    cmd.Parameters.Add(new SqlParameter("CustomerEmail", agreement.displayParticipantSetInfos.First().displayUserSetMemberInfos.First().email));
                                }

                                conn.Open();
                                var obj = cmd.ExecuteScalar();
                                if (obj != null)
                                {
                                    applicantId = (int)obj;
                                    companyID = (int)ConstantsHelper.Company.JUST_BUDGET;
                                }
                                else
                                    applicantId = 0;
                            }

                            // Save agreement in our database
                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.CONNECTION_STRING_Marketing_MM]))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append("INSERT INTO Agreement ");
                                sql.Append("(AgreementId, ApplicantId, CustomerEmail, ApplicationManagerEmail, AgreementStatus, CompanyID) ");
                                sql.Append("SELECT ");
                                sql.Append("@AgreementId, @ApplicantId, @CustomerEmail, @ApplicationManagerEmail, @AgreementStatus, @CompanyID ");
                                sql.Append("WHERE NOT EXISTS (SELECT AgreementId FROM Agreement WHERE AgreementId = @AgreementId) ");

                                SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

                                cmd.Parameters.Add(new SqlParameter("AgreementId", agreementInfo.Id));
                                cmd.Parameters.Add(new SqlParameter("ApplicantId", applicantId));

                                if (agreementInfo.participantSetsInfo != null && agreementInfo.participantSetsInfo.Count > 0 &&
                                    agreementInfo.participantSetsInfo.First().memberInfos != null && agreementInfo.participantSetsInfo.First().memberInfos.Count > 0)
                                {
                                    cmd.Parameters.Add(new SqlParameter("CustomerEmail", agreementInfo.participantSetsInfo.First().memberInfos.First().email));
                                }
                                else
                                {
                                    cmd.Parameters.Add(new SqlParameter("CustomerEmail", agreement.displayParticipantSetInfos.First().displayUserSetMemberInfos.First().email));
                                }

                                cmd.Parameters.Add(new SqlParameter("ApplicationManagerEmail", agreementInfo.ccs == null ? string.Empty : agreementInfo.ccs.First().email));
                                cmd.Parameters.Add(new SqlParameter("AgreementStatus", agreementInfo.status));
                                cmd.Parameters.Add(new SqlParameter("CompanyID", companyID));

                                conn.Open();
                                cmd.ExecuteNonQuery();

                                // Check if the agreement created date (application pack sent out) and signed date (application pack received)
                                if (eventList != null & eventList.events != null && eventList.events.Count > 0 &&
                                    eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.CREATED)) != null)
                                {
                                    sql = sql.Clear();
                                    cmd = new SqlCommand();
                                    cmd.Connection = conn;

                                    sql.Append("UPDATE Agreement SET ");
                                    sql.Append("ApplicationPackageSentDate = @ApplicationPackageSentDate ");

                                    cmd.Parameters.Add(new SqlParameter("ApplicationPackageSentDate", TimeZoneInfo.ConvertTime(eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.CREATED)).date, timeZone)));

                                    if (eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.ACTION_COMPLETED_HOSTED)) != null)
                                    {
                                        sql.Append(", ApplicationPackageReceivedDate = @ApplicationPackageReceivedDate ");
                                        cmd.Parameters.Add(new SqlParameter("ApplicationPackageReceivedDate", TimeZoneInfo.ConvertTime(eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.ACTION_COMPLETED_HOSTED)).date, timeZone)));
                                    }
                                    else if (eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.ACTION_COMPLETED)) != null)
                                    {
                                        sql.Append(", ApplicationPackageReceivedDate = @ApplicationPackageReceivedDate ");
                                        cmd.Parameters.Add(new SqlParameter("ApplicationPackageReceivedDate", TimeZoneInfo.ConvertTime(eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.ACTION_COMPLETED)).date, timeZone)));
                                    }
                                    else if (eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.SIGNED)) != null)
                                    {
                                        sql.Append(", ApplicationPackageReceivedDate = @ApplicationPackageReceivedDate ");
                                        cmd.Parameters.Add(new SqlParameter("ApplicationPackageReceivedDate", TimeZoneInfo.ConvertTime(eventList.events.Find(e => e.type == nameof(Enums.AgreementEventType.SIGNED)).date, timeZone)));
                                    }

                                    sql.Append("WHERE AgreementId = @AgreementId ");
                                    cmd.Parameters.Add(new SqlParameter("AgreementId", agreementInfo.Id));

                                    cmd.CommandText = sql.ToString();
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        else
                        {
                            LogHelper.WriteLog("Agreement details not found.", true, 5);
                        }
                    }
                    catch (Exception error)
                    {
                        LogHelper.WriteLogSeparator();
                        LogHelper.WriteLog(error.Message, false, 1);
                        LogHelper.WriteLogSeparator();
                    }
                }
            }
            else
            {
                LogHelper.WriteLog("Agreements not found.", true, 3);
            }
        }

        public static string truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}