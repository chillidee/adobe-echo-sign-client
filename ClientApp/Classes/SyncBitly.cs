﻿using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using IO = System.IO;

namespace ClientApp.Classes
{
    public class SyncBitly
    {
        public static string GetBitly(string longUrl)
        {
            string shortUrl = string.Empty;

            var bitlyUrl = string.Format(ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.BITLY_URL], HttpUtility.UrlEncode(longUrl),
                                    ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.BITLY_LOGIN], ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.BITLY_API_KEY]);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(bitlyUrl);
            try
            {
                WebResponse response = request.GetResponse();
                using (IO.Stream responseStream = response.GetResponseStream())
                {
                    IO.StreamReader reader = new IO.StreamReader(responseStream, Encoding.UTF8);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic jsonResponse = js.Deserialize<dynamic>(reader.ReadToEnd());
                    shortUrl = jsonResponse["results"][longUrl]["shortUrl"];
                }
            }
            catch (WebException error)
            {
                WebResponse errorResponse = error.Response;
                using (IO.Stream responseStream = errorResponse.GetResponseStream())
                {
                    IO.StreamReader reader = new IO.StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    throw new Exception(reader.ReadToEnd());
                }
            }
            return shortUrl;
        }
    }
}
