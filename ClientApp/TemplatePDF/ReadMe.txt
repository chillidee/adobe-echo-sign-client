******************************************************************************************

Author: 		Rodrigo E. Boni

Creation Date: 	06/12/2018
Description:	Instructions to change Clean Credit "Authority to Act & DDR Agreement.pdf" 
Updated Date: 	15/03/2018
Version:		1.3

******************************************************************************************

Please follow these steps:


1) Go to the website https://adminconsole.adobe.com/enterprise/


	Email Adress:	sales@cleancredit.com.au

	Password:	********


2) Click in "Manage Adobe Sign" under Overview > Products


3) Now go to "Manage > Library Template" if you want to edit active input fields, 

   Or go to "Dashboard > Add Template to Library" if you want to create a new template, do not forget to drag & drop the PDF which you want to work on, like "Authority to Act & DDR Agreement.pdf"


*  If you wish to change the content, unfortunately (because Adobe Echo Sign is not powerful)
   you have to change the Word Document, Export As PDF, Add Template to Library, Drag & Drop Input Fields (rebuild) 

   - Keep these fields with the same old names in order to be parameterizable

"fieldName":"customerName"
"fieldName":"customerReference"
"fieldName":"customerSurname"
"fieldName":"customerGivenName"
"fieldName":"customerMobile"
"fieldName":"accountBSB"
"fieldName":"accountNumber"
"fieldName":"accountHolderName"
"fieldName":"applicationFee"
"fieldName":"removalFee"   
"fieldName":"removalFeeNo"
"fieldName":"judgementFee"