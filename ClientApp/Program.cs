﻿using EchosignRESTClient.Parameters;
using Newtonsoft.Json;
using System;
using System.Configuration;
using ClientApp.Classes;
using IO = System.IO;

namespace ClientApp
{
    /// <summary>
    /// *********************************************************************************************************************
    /// 
    /// Author:             Rodrigo Boni
    /// 
    /// Creation Date:      07/12/2018
    /// 
    /// Update Date:        17/02/2020
    /// 
    /// Version:            1.1.0
    /// 
    /// Description:        Client that consumes Adobe Echo Sign API
    ///                     It uses Library Document to create Agreement and sends it to the customer in order to get it signed
    ///                     Then Clean Credit has to approve it, both will receive emails with the document attached
    /// 
    /// Resources:          https://au1.documents.adobe.com/public/docs/restapi/v6
    ///                     https://au1.documents.adobe.com/public/static/oauthDoc.jsp
    ///                     https://github.com/adobe-sign
    ///                     https://forums.adobe.com/welcome
    ///                     
    /// Final notes:        You are good to go mate! Hang in there...
    /// 
    /// *********************************************************************************************************************
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                // Set log location
                LogHelper.LogPath = ConfigurationManager.AppSettings[ConstantsHelper.AppConfig.LOG_PATH];

                LogHelper.WriteLogSeparator(true);
                LogHelper.WriteLog("ClientApp has started running.");

                if (args == null || args.Length == 0 || !args[ConstantsHelper.CommandLine.ACTION].StartsWith("action="))
                {
                    LogHelper.WriteLog("Please inform the command-line argument action=<MethodName> before executing client.", false, 1);
                }
                else if (args.Length < 2 || !args[1].StartsWith("json=") || !IO.File.Exists(args[ConstantsHelper.CommandLine.JSON].Replace("json=", "")))
                {
                    LogHelper.WriteLog("Please inform the command-line argument json=<FilePath> before executing client.", false, 1);
                }
                else
                {
                    LogHelper.WriteLog("1. Checking command-line arguments...", true, 1);

                    String action = args[ConstantsHelper.CommandLine.ACTION].Replace("action=", "");
                    JsonData jsonData = JsonConvert.DeserializeObject<JsonData>(IO.File.ReadAllText(args[ConstantsHelper.CommandLine.JSON].Replace("json=", "")));

                    LogHelper.WriteLog(string.Format("Action: {0}", action), false, 2);
                    LogHelper.WriteLog(string.Format("Json File: {0}", args[ConstantsHelper.CommandLine.JSON].Replace("json=", "")), false, 2);

                    if (string.Equals(action, ConstantsHelper.Action.SEND_SMS, StringComparison.OrdinalIgnoreCase))
                        SyncEsendex.Execute().Wait();
                    else                    
                        SyncAdobeEchoSign.Execute(action, jsonData).Wait();                    
                }
            }
            catch (Exception error)
            {
                LogHelper.WriteLogSeparator();
                LogHelper.WriteLog(error.Message, false, 1);
                LogHelper.WriteLogSeparator();
            }
            finally
            {
                LogHelper.WriteLog("ClientApp has finished running.", true);
            }
        }
    }
}