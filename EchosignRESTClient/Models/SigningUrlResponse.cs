﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    public class SigningUrlResponse
    {
        /// <summary>
        /// An array of urls for signer sets involved in this agreement
        /// </summary>
        public List<SigningUrlSetInfo> signingUrlSetInfos { get; set; }
    }
}