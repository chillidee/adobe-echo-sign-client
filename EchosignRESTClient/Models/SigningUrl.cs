﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    public class SigningUrl
    {
        /// <summary>
        /// The email address of the signer associated with this signing url
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// The email address of the signer associated with this signing url
        /// </summary>
        public string esignUrl { get; set; }
    }
}
