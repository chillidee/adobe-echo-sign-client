﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    public class UserAgreements
    {
        /// <summary>
        /// An array of UserAgreement items
        /// </summary>
        public List<UserAgreement> userAgreementList { get; set; }

        /// <summary>
        /// Pagination information for navigating through the response
        /// </summary>
        public PageInfo page { get; set; }
    }
}
