﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    public class EventList
    {
        /// <summary>
        /// An ordered list of the events in the audit trail of this document
        /// </summary>
        public List<Event> events { get; set; }
    }
}
