﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    /// <summary>
    /// Information about the agreement that you want to send and authoring options that you want to apply at the time of sending
    /// </summary>
    public class SendAgreementRequest
    {
        /// <summary>
        /// The name of the document, specified by the sender
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// An appropriate message to the recipient
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// A list of one or more email addresses that you want to copy on this transaction. 
        /// The email addresses will each receive an email at the beginning of the transaction 
        /// and also when the final document is signed. The email addresses will also receive a 
        /// copy of the document, attached as a PDF file
        /// </summary>
        public List<string> ccs { get; set; }

        /// <summary>
        /// Information about all the participant sets of this document
        /// </summary>
        public List<ParticipantSetInfo> participantSetsInfo { get; set; }

        /// <summary>
        /// ['ESIGN' or 'WRITTEN']: Specifies the type of signature you would like to request - written or e-signature
        /// </summary>
        public string signatureType { get; set; }

        /// <summary>
        /// The valid state of agreement (IN_PROCESS/AUTHORING/DRAFT)
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// A list of one or more files (or references to files) that will be sent out for signature. 
        /// If more than one file is provided, they will be combined into one PDF before being sent out. 
        /// Note: Only one of the four parameters in every FileInfo object must be specified
        /// </summary>
        public List<FileInfo> fileInfos { get; set; }

        /// <summary>
        /// Default value for merge fields in agreement document
        /// </summary>
        public List<MergefieldInfo> mergeFieldInfo { get; set; }
    }
}
