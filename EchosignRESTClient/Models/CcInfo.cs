﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    public class CcInfo
    {
        public string email { get; set; }
        public string label { get; set; }
        public List<string> visiblePages { get; set; }
    }
}
