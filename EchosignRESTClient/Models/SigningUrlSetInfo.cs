﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchosignRESTClient.Models
{
    public class SigningUrlSetInfo
    {
        /// <summary>
        /// An array of urls for current signer set
        /// </summary>
        public List<SigningUrl> signingUrls { get; set; }

        /// <summary>
        /// The name of the current signer set. Returned only, if the API caller is the sender of agreement
        /// </summary>
        public string signingUrlSetName { get; set; }       
    }
}