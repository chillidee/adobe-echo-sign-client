﻿using EchosignRESTClient.Models;
using System.Collections.Generic;

namespace EchosignRESTClient.Parameters
{
    public class JsonData
    {
        /// <summary>
        /// Application under this company
        /// </summary>
        public int companyID { get; set; }

        /// <summary>
        /// A list of one or more email addresses that you want to copy on this transaction. 
        /// The email addresses will each receive an email at the beginning of the transaction 
        /// and also when the final document is signed. The email addresses will also receive a 
        /// copy of the document, attached as a PDF file
        /// </summary>
        public List<string> ccs { get; set; }

        /// <summary>
        /// Information about the members of the recipient set
        /// </summary>
        public List<ParticipantInfo> memberInfos { get; set; }

        /// <summary>
        /// Default value for merge fields in agreement document
        /// </summary>
        public List<MergefieldInfo> mergeFieldInfo { get; set; }
    }
}
